package io.github.meta.ease.hsmsoftware.core.util;

public class StringUtil {
    private StringUtil() {
    }

    public static boolean hasLength(String str) {
        return (str != null && !str.isEmpty());
    }
}