package io.github.meta.ease.hsmsoftware.core;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.SecureRandom;
import java.security.Security;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class GMBaseUtil {

    private static final String PROV_NAME = BouncyCastleProvider.PROVIDER_NAME;

    protected final Lock lock = new ReentrantLock();

    protected final SecureRandom random = new SecureRandom();

    static {
        if (Security.getProperty(PROV_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }
}
