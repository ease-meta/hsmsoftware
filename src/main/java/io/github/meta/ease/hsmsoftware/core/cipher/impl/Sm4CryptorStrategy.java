package io.github.meta.ease.hsmsoftware.core.cipher.impl;

import io.github.meta.ease.hsmsoftware.core.GMBaseUtil;
import io.github.meta.ease.hsmsoftware.core.cipher.CryptorStrategy;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Objects;

public class Sm4CryptorStrategy extends GMBaseUtil implements CryptorStrategy {

    private static final String CIPHER_PARAM = "SM4";

    private static final String MODE_PARAM = "SM4/ECB/PKCS7Padding";

    private static final String PROV_NAME = BouncyCastleProvider.PROVIDER_NAME;

    private Cipher cipher;

    @Override
    public byte[] encrypt(byte[] data, byte[] key) {
        lock.lock();
        try {
            Cipher encryptCipher = getCipher();
            Key sm4Key = generateSm4Key(key);
            encryptCipher.init(Cipher.ENCRYPT_MODE, sm4Key);
            return encryptCipher.doFinal(data);
        } catch (IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException | NoSuchProviderException |
                 NoSuchPaddingException | InvalidKeyException e) {
            System.err.println("SM4加密时出现异常" + e.getMessage());
            return null;
        } finally {
            lock.unlock();
        }
    }


    @Override
    public byte[] decrypt(byte[] cipherData, byte[] key) {
        lock.lock();
        try {
            Cipher decryptCipher = getCipher();
            Key sm4Key = generateSm4Key(key);
            decryptCipher.init(Cipher.DECRYPT_MODE, sm4Key);
            return decryptCipher.doFinal(cipherData);
        } catch (Exception e) {
            System.err.println("SM4解密时出现异常" + e.getMessage());
            return null;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public String getName() {
        return CIPHER_PARAM;
    }

    private Cipher getCipher() throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException {
        if (null == this.cipher) {
            this.cipher = Cipher.getInstance(MODE_PARAM, PROV_NAME);
        }
        Objects.requireNonNull(this.cipher, "cipher must be not null !");
        return this.cipher;
    }

    private static Key generateSm4Key(byte[] key) {
        return new SecretKeySpec(key, CIPHER_PARAM);
    }
}
