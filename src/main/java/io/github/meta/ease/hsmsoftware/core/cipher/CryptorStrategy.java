package io.github.meta.ease.hsmsoftware.core.cipher;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public interface CryptorStrategy {
    default byte[] decrypt(final byte[] cipherData, final byte[] key) {
        throw new NotImplementedException();
    }

    default byte[] encrypt(final byte[] data, final byte[] key) {
        throw new NotImplementedException();
    }

    String getName();
}
