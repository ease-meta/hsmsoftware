package io.github.meta.ease.hsmsoftware.core.cipher.impl;

import io.github.meta.ease.hsmsoftware.core.GMBaseUtil;
import io.github.meta.ease.hsmsoftware.core.cipher.SignStrategy;
import io.github.meta.ease.hsmsoftware.core.util.KeyGenUtil;
import io.github.meta.ease.hsmsoftware.core.util.Pair;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.util.encoders.Hex;

import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.util.Arrays;
import java.util.Objects;

public class SM3WithSM2SignStrategy extends GMBaseUtil implements SignStrategy {

    private static final String SIGNATURE_PARAM = "SM3withSM2";

    private static final int RS_LEN = 32;

    protected Signature signature;

    @Override
    public byte[] sign(final byte[] srcData, final byte[] privateKeyData) {
        lock.lock();
        try {
            PrivateKey privateKey = KeyGenUtil.generatePrivateKey(privateKeyData);
            return signature(srcData, privateKey);
        } catch (Exception e) {
            return null;
        } finally {
            lock.unlock();
        }
    }


    @Override
    public Pair<Boolean, String> verify(byte[] srcData, byte[] sign, byte[] publicKeyData) {
        boolean b = false;
        lock.lock();
        try {
            PublicKey publicKey = KeyGenUtil.generatePublicKey(publicKeyData);
            byte[] sign_asn1 = rsPlainByteArrayToAsn1(sign);
            b = verifySignature(srcData, sign_asn1, publicKey);
            return Pair.of(b, new String(srcData));
        } catch (Exception e) {
            return Pair.of(b, new String(srcData));
        } finally {
            lock.unlock();
        }
    }

    private byte[] signature(byte[] srcData, PrivateKey privateKey) throws Exception {
        Signature signature = getSignature();
        signature.initSign(privateKey, random);
        signature.update(srcData);
        byte[] sign = signature.sign();
        return rsAsn1ToPlainByteArray(sign);
    }

    private boolean verifySignature(byte[] srcData, byte[] sign, PublicKey publicKey) throws Exception {
        Signature signature = getSignature();
        signature.initVerify(publicKey);
        signature.update(srcData);
        return signature.verify(sign);
    }

    private byte[] rsPlainByteArrayToAsn1(byte[] sign) {
        if (sign.length != RS_LEN * 2) throw new RuntimeException("err rs. ");
        BigInteger r = new BigInteger(1, Arrays.copyOfRange(sign, 0, RS_LEN));
        BigInteger s = new BigInteger(1, Arrays.copyOfRange(sign, RS_LEN, RS_LEN * 2));
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(new ASN1Integer(r));
        v.add(new ASN1Integer(s));
        try {
            return new DERSequence(v).getEncoded("DER");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private byte[] rsAsn1ToPlainByteArray(byte[] rsDer) {
        ASN1Sequence seq = ASN1Sequence.getInstance(rsDer);
        byte[] r = bigIntToFixexLengthBytes(ASN1Integer.getInstance(seq.getObjectAt(0)).getValue());
        byte[] s = bigIntToFixexLengthBytes(ASN1Integer.getInstance(seq.getObjectAt(1)).getValue());
        byte[] result = new byte[RS_LEN * 2];
        System.arraycopy(r, 0, result, 0, r.length);
        System.arraycopy(s, 0, result, RS_LEN, s.length);
        return result;
    }

    private byte[] bigIntToFixexLengthBytes(BigInteger rOrS) {
        byte[] rs = rOrS.toByteArray();
        if (rs.length == RS_LEN) return rs;
        else if (rs.length == RS_LEN + 1 && rs[0] == 0) return Arrays.copyOfRange(rs, 1, RS_LEN + 1);
        else if (rs.length < RS_LEN) {
            byte[] result = new byte[RS_LEN];
            Arrays.fill(result, (byte) 0);
            System.arraycopy(rs, 0, result, RS_LEN - rs.length, rs.length);
            return result;
        } else {
            throw new RuntimeException("err rs: " + Hex.toHexString(rs));
        }
    }

    private Signature getSignature() throws NoSuchAlgorithmException {
        if (null == this.signature) {
            this.signature = Signature.getInstance(SIGNATURE_PARAM);
        }
        Objects.requireNonNull(this.signature, "signature must be not null !");
        return this.signature;
    }

    @Override
    public String getName() {
        return SIGNATURE_PARAM;
    }
}
