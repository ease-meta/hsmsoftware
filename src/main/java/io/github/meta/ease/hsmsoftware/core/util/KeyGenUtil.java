package io.github.meta.ease.hsmsoftware.core.util;

import io.github.meta.ease.hsmsoftware.core.GMBaseUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class KeyGenUtil extends GMBaseUtil {
    public static KeyPair generateSm2KeyPair() {
        try {
            final ECGenParameterSpec sm2Spec = new ECGenParameterSpec("sm2p256v1");
            //获取一个椭圆曲线类型的密钥对生成器
            final KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC", new BouncyCastleProvider());
            SecureRandom random = new SecureRandom();
            //使用sm2的算法区域初始化密钥生成器
            kpg.initialize(sm2Spec, random);
            KeyPair keyPair = kpg.generateKeyPair();
            return keyPair;
        } catch (Exception e) {
            throw new RuntimeException("生成密钥对失败");
        }
    }

    public static PrivateKey generatePrivateKey(byte[] key) throws InvalidKeySpecException, NoSuchAlgorithmException {
        KeyFactory keyFactory = getKeyFactory("EC");
        return keyFactory.generatePrivate(new PKCS8EncodedKeySpec(key));
    }

    public static PublicKey generatePublicKey(byte[] key) throws InvalidKeySpecException, NoSuchAlgorithmException {
        KeyFactory keyFactory = getKeyFactory("EC");
        return keyFactory.generatePublic(new X509EncodedKeySpec(key));
    }

    private static KeyFactory getKeyFactory(String algorithm) throws NoSuchAlgorithmException {
        return KeyFactory.getInstance(algorithm);
    }

    public static void main(String[] args) {
        KeyPair keyPair = generateSm2KeyPair();
        PublicKey publicKey = keyPair.getPublic();
        System.out.println("BASE64格式SM2公钥：" + Base64.toBase64String(publicKey.getEncoded()));
        PrivateKey privateKey = keyPair.getPrivate();
        System.out.println("BASE64格式SM2私钥：" + Base64.toBase64String(privateKey.getEncoded()));
    }
}