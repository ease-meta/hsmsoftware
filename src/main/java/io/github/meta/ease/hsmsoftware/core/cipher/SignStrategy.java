package io.github.meta.ease.hsmsoftware.core.cipher;


import io.github.meta.ease.hsmsoftware.core.exception.NotImplementedException;
import io.github.meta.ease.hsmsoftware.core.util.Pair;

public interface SignStrategy {


    default byte[] sign(final byte[] data, final byte[] privateKeyData) {
        throw new NotImplementedException();
    }


    default Pair<Boolean, String> verify(final byte[] data, final byte[] sign, final byte[] publicKeyData) {
        throw new NotImplementedException();
    }


    String getName();
}
