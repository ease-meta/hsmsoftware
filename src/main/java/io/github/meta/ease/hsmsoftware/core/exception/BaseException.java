package io.github.meta.ease.hsmsoftware.core.exception;

abstract class BaseException extends RuntimeException {

    private static final long serialVersionUID = -5896431877288268263L;

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
