package io.github.meta.ease.hsmsoftware.core.cipher.impl;

import io.github.meta.ease.hsmsoftware.core.GMBaseUtil;
import io.github.meta.ease.hsmsoftware.core.cipher.CryptorStrategy;
import io.github.meta.ease.hsmsoftware.core.util.KeyGenUtil;
import org.bouncycastle.crypto.engines.SM2Engine;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Objects;

public class Sm2CryptorStrategy extends GMBaseUtil implements CryptorStrategy {

    private final SM2Engine.Mode mode = SM2Engine.Mode.C1C3C2;

    private SM2Engine engine;

    @Override
    public byte[] encrypt(byte[] data, byte[] publicKeyData) {
        lock.lock();
        try {
            SM2Engine encryptEngine = getEngine();
            ECPublicKeyParameters ecPublicKeyParameters = parseSM2PublicKey(publicKeyData);
            encryptEngine.init(true, new ParametersWithRandom(ecPublicKeyParameters, random));
            return encryptEngine.processBlock(data, 0, data.length);
        } catch (Exception e) {
            System.err.println("SM2加密时出现异常" + e.getMessage());
            return null;
        } finally {
            lock.unlock();
        }

    }

    @Override
    public byte[] decrypt(byte[] cipherData, byte[] privateKeyData) {
        lock.lock();
        try {
            ECPrivateKeyParameters ecPrivateKeyParameters = parseSM2PrivateKey(privateKeyData);
            SM2Engine decryptEngine = getEngine();
            decryptEngine.init(false, ecPrivateKeyParameters);
            return decryptEngine.processBlock(cipherData, 0, cipherData.length);
        } catch (Exception e) {
            System.err.println("SM2解密时出现异常" + e.getMessage());
            return null;
        } finally {
            lock.unlock();
        }

    }

    @Override
    public String getName() {
        return "SM2";
    }

    private SM2Engine getEngine() {
        if (null == this.engine) {
            this.engine = new SM2Engine(mode);
        }
        Objects.requireNonNull(this.engine, "engine must be not null !");
        return this.engine;
    }

    private ECPrivateKeyParameters parseSM2PrivateKey(byte[] privateKeyBytes) throws InvalidKeySpecException, InvalidKeyException, NoSuchAlgorithmException {
        PrivateKey privateKey = KeyGenUtil.generatePrivateKey(privateKeyBytes);
        return (ECPrivateKeyParameters) ECUtil.generatePrivateKeyParameter(privateKey);
    }

    private ECPublicKeyParameters parseSM2PublicKey(byte[] publicKeyBytes) throws InvalidKeySpecException, InvalidKeyException, NoSuchAlgorithmException {
        PublicKey publicKey = KeyGenUtil.generatePublicKey(publicKeyBytes);
        return (ECPublicKeyParameters) ECUtil.generatePublicKeyParameter(publicKey);
    }
}
