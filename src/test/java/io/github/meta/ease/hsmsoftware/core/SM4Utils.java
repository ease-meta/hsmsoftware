package io.github.meta.ease.hsmsoftware.core;

import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.engines.SM4Engine;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.paddings.ZeroBytePadding;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.util.encoders.Hex;

public class SM4Utils {
    private static final String SM4_ENCRYPTION_ALGORITHM = "SM4/CBC/PKCS5Padding";
    private static final String SM4_KEY_ALGORITHM = "SM4";
    private static final int SM4_KEY_SIZE = 128; // in bits
    private static final int SM4_BLOCK_SIZE = 128; // in bits
    private static final int SM4_IV_LENGTH = 16; // in bytes

    public static String encrypt(byte[] key, byte[] data) throws Exception {
        byte[] iv = new byte[SM4_IV_LENGTH]; // IV is fixed to all zeroes for now
        KeyParameter keyParameter = new KeyParameter(key);
        BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new SM4Engine(), new ZeroBytePadding());
        cipher.init(true, new ParametersWithIV(keyParameter, iv));
        byte[] output = new byte[cipher.getOutputSize(data.length)];
        int len = cipher.processBytes(data, 0, data.length, output, 0);
        cipher.doFinal(output, len);
        return Hex.toHexString(output);
    }

    public static String decrypt(byte[] key, byte[] encryptedData) throws Exception {
        byte[] iv = new byte[SM4_IV_LENGTH]; // IV is fixed to all zeroes for now
        KeyParameter keyParameter = new KeyParameter(key);
        BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new SM4Engine(), new ZeroBytePadding());
        cipher.init(false, new ParametersWithIV(keyParameter, iv));
        byte[] output = new byte[cipher.getOutputSize(encryptedData.length)];
        int len = cipher.processBytes(encryptedData, 0, encryptedData.length, output, 0);
        cipher.doFinal(output, len);
        return Hex.toHexString(output);
    }
}