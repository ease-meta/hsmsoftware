package io.github.meta.ease.hsmsoftware.core.util;

import org.junit.jupiter.api.Test;

/**
 * <p>文件名称:  NonceUtilTest</p>
 * <p>描述:     </p>
 * <p>创建时间:  2024/1/5</p>
 *
 * @author Abu
 * @version 22.1.0
 * @since 22.1.0
 */
class NonceUtilTest {
    @Test
    void createNonce() {
        System.out.println(NonceUtil.createNonce(6));
    }
}