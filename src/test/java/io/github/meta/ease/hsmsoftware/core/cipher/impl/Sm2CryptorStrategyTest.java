package io.github.meta.ease.hsmsoftware.core.cipher.impl;

import org.bouncycastle.util.encoders.Base64;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

/**
 * <p>文件名称:  Sm2CryptorStrategyTest</p>
 * <p>描述:     </p>
 * <p>创建时间:  2024/1/5</p>
 *
 * @author Abu
 * @version 22.1.0
 * @since 22.1.0
 */
class Sm2CryptorStrategyTest {

    //BASE64格式的公私钥
    String pub = "MFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAENLxzZ1nEP9CC8K+kE3mmZYTHZt57OmVxJ7buwhWDMP5LaBrJ+X7V08v87qEGsKZF1OtOuMSn8g/iH3EnrBRx5w==";
    String pri = "MIGTAgEAMBMGByqGSM49AgEGCCqBHM9VAYItBHkwdwIBAQQgtDWjaNZOcVrrF/AWgBLu1XFhLsoovuFRcX36k6uvfhWgCgYIKoEcz1UBgi2hRANCAAQ0vHNnWcQ/0ILwr6QTeaZlhMdm3ns6ZXEntu7CFYMw/ktoGsn5ftXTy/zuoQawpkXU6064xKfyD+IfcSesFHHn";

    @Test
    void encrypt() {
        Sm2CryptorStrategy sm2CryptorStrategy = new Sm2CryptorStrategy();
        byte[] cipherData = sm2CryptorStrategy.encrypt("https://www.baidu.com".getBytes(StandardCharsets.UTF_8), Base64.decode(pub));

        byte[] decrypt = sm2CryptorStrategy.decrypt(cipherData, Base64.decode(pri));
        Assertions.assertEquals("https://www.baidu.com", new String(decrypt));
    }
}