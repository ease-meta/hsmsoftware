package io.github.meta.ease.hsmsoftware.core.cipher.impl;

import io.github.meta.ease.hsmsoftware.core.util.NonceUtil;
import org.bouncycastle.util.encoders.Base64;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

class Sm4CryptorStrategyTest {

    String key = NonceUtil.createNonce(16);

    @Test
    void encrypt() {
        Sm4CryptorStrategy sm4CryptorStrategy = new Sm4CryptorStrategy();
        byte[] encrypt = sm4CryptorStrategy.encrypt("https://www.baidu.com".getBytes(StandardCharsets.UTF_8), key.getBytes(StandardCharsets.UTF_8));
        System.out.println(Base64.toBase64String(encrypt));

        byte[] decrypt = sm4CryptorStrategy.decrypt(encrypt, key.getBytes());
        System.out.println(new String(decrypt));

    }
}