package io.github.meta.ease.hsmsoftware.core.cipher.impl;

import io.github.meta.ease.hsmsoftware.core.util.Pair;
import org.bouncycastle.util.encoders.Base64;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SM3WithSM2SignStrategyTest {
    //BASE64格式的公私钥
    String pub = "MFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAENLxzZ1nEP9CC8K+kE3mmZYTHZt57OmVxJ7buwhWDMP5LaBrJ+X7V08v87qEGsKZF1OtOuMSn8g/iH3EnrBRx5w==";
    String pri = "MIGTAgEAMBMGByqGSM49AgEGCCqBHM9VAYItBHkwdwIBAQQgtDWjaNZOcVrrF/AWgBLu1XFhLsoovuFRcX36k6uvfhWgCgYIKoEcz1UBgi2hRANCAAQ0vHNnWcQ/0ILwr6QTeaZlhMdm3ns6ZXEntu7CFYMw/ktoGsn5ftXTy/zuoQawpkXU6064xKfyD+IfcSesFHHn";

    @Test
    void signAndVerify() {

        SM3WithSM2SignStrategy sm3WithSM2SignStrategy = new SM3WithSM2SignStrategy();
        byte[] sign = sm3WithSM2SignStrategy.sign("https://www.baidu.com".getBytes(), Base64.decode(pri));

        Pair<Boolean, String> verifyFalse = sm3WithSM2SignStrategy.verify("https://www.baidu.com1".getBytes(), sign, Base64.decode(pub));

        Assertions.assertFalse(verifyFalse.getFirst());

        Pair<Boolean, String> verifyTrue = sm3WithSM2SignStrategy.verify("https://www.baidu.com".getBytes(), sign, Base64.decode(pub));

        Assertions.assertTrue(verifyTrue.getFirst());
    }

}