国密算法是中国国家密码标准，包括SM2、SM3、SM4等一系列算法。BouncyCastle是一个开源的密码学库，支持多种密码算法，包括国密算法。

以下是在Java中使用BouncyCastle实现SM4加解密、SM2签名验签的步骤：

### 1. 引入BouncyCastle库

首先，确保你的项目中已经引入了BouncyCastle的依赖。如果你使用Maven，可以在`pom.xml`中添加以下依赖：

```xml

<dependency>
    <groupId>org.bouncycastle</groupId>
    <artifactId>bcpg-jdk18on</artifactId>
    <version>1.77</version> <!-- 请检查最新版本 -->
</dependency>
```

### 2. SM4加解密

SM4是一个分组密码，其密钥长度和分组长度都为128位。

#### 加密：

```java
import org.bouncycastle.crypto.engines.SM4Engine;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.paddings.ZeroBytePadding;
import org.bouncycastle.util.encoders.Hex;

public class SM4Utils {
    private static final String SM4_ENCRYPTION_ALGORITHM = "SM4/CBC/PKCS5Padding";
    private static final String SM4_KEY_ALGORITHM = "SM4";
    private static final int SM4_KEY_SIZE = 128; // in bits
    private static final int SM4_BLOCK_SIZE = 128; // in bits
    private static final int SM4_IV_LENGTH = 16; // in bytes

    public static String encrypt(byte[] key, byte[] data) throws Exception {
        byte[] iv = new byte[SM4_IV_LENGTH]; // IV is fixed to all zeroes for now
        KeyParameter keyParameter = new KeyParameter(key);
        BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new SM4Engine(), new ZeroBytePadding());
        cipher.init(true, new ParametersWithIV(keyParameter, iv));
        byte[] output = new byte[cipher.getOutputSize(data.length)];
        int len = cipher.processBytes(data, 0, data.length, output, 0);
        cipher.doFinal(output, len);
        return Hex.toHexString(output);
    }
}
```

#### 解密：

```java
public static String decrypt(byte[] key, byte[] encryptedData) throws Exception {
    byte[] iv = new byte[SM4_IV_LENGTH]; // IV is fixed to all zeroes for now
    KeyParameter keyParameter = new KeyParameter(key);
    BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new SM4Engine(), new ZeroBytePadding());
    cipher.init(false, new ParametersWithIV(keyParameter, iv));
    byte[] output = new byte[cipher.getOutputSize(encryptedData.length)];
    int len = cipher.processBytes(encryptedData, 0, encryptedData.length, output, 0);
    cipher.doFinal(output, len);
    return Hex.toHexString(output);
}
```